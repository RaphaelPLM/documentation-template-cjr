## Visão geral da API
> *OBSERVAÇÃO: Apague os textos que estão em blocos de citação*

> Nesse parágrafo, cite qual o projeto em questão, em **negrito**, e
> faça uma breve descrição dos serviços da API.
> 
> Insira aqui, opcionalmente, algum comentário extra a respeito da 
> arquitetura da API, uma observação geral do projeto, etc...

### Persistência de dados

> Cite todos os serviços de armazenamento de dados nesta seção. Vale
> ressaltar que serviços de terceiros, como as nuvens Amazon S3 e Google
> Cloud Storage devem ser mencionados caso utilizados.

### Módulos externos

Foram utilizados os seguintes módulos externos para desenvolvimento:
> Descreva somente os **módulos principais**. Não é necessário mencionar cada uma das gemas de um projeto Rails, ou pacotes de um projeto Node.js, por exemplo. Utilize a tabela abaixo como base, e altere os campos com as informações corretas.

Módulo | Proposta
:--:|:--:
Módulo 1 | Breve descrição
Módulo 2| Breve descrição

### Visão geral das entidades

As entidades de bancos de dados utilizadas na aplicação foram as seguintes:

> Mencione todas as entidades do banco de dados. Não é necessário explicitar os relacionamentos entre elas, visto que isso é expresso no modelo ER. Todavia, é opcional e fica a critério da equipe colocar uma descrição breve ou detalhada sobre as entidades.

- **Entidade 1**: breve descrição opcional;
- **Entidade 2**
- **Entidade 3**

Uma visão mais descritiva sobre as entidades e seus relacionamentos pode ser encontrada em um diagrama, no link: 

> Coloque o link abaixo do modelo Entidade-Relacionamento, ou similar (contanto que descreva o banco de dados de forma didática) do projeto. O serviço de hospedagem da imagem fica a critério da equipe. Um exemplo é fornecido abaixo.

> https://app.diagrams.net/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Untitled%20Diagram.drawio#R7VvbUtswEP2aPNLxLU54xCnQFtpSQktfRaTYKrLkykpD%2BPrKseRLRC6mQOKOZ%2FJgrderaPfsevdk0nNH8cM5B0n0mUFEeo4FH3ru%2B57j2J7j9LKPBRe5ZGh5uSDkGCqlUjDGj0gJLSWdYYjSmqJgjAic1IUTRimaiJoMcM7mdbUpI%2FVdExAiQzCeAGJKbzEUkTqFMyjlHxAOI72z7R%2Fnd2KgldVJ0ghANq%2BI3NOeO%2BKMifwqfhghkjlP%2B%2BX24%2BKWXN7755%2B%2Bpb%2FB9%2BDi5suPo9zYWZNHiiNwRMWzTT%2FeT88%2B%2FPB%2B%2FUyuzr7NrfOTu09HR31l%2Bw8gM%2BWw0SwVLEZcHVostCfTOY4JoHIVTBkVY3XHlutJhAm8BAs2y75fKsDkXq%2BCiHH8KPUBUcryNhcKKI5f0xhnT0qxJaUcpVLnSh%2FaLkSXIBVKZ8IIAUmK74pvEgMeYhowIc%2BgDbEZhQiqVRHF5UJwdl%2FgInt%2BR1drtyEu0EMFaMr150h6UPCFVNF3fYUilUa2RtW8BGWhE1UAORiqXFB5EBami92uZd4AGkofFNs59d2GO24mw1HbDBCBOAUCBZkP0yq%2B5EXlnKVoibomCHTWIvAjNDEYgSS7lAESGJDy5G4gWKKiStBUA4Srs2XXdxoUWZwBwSGV17lukAUSy6JxosRLY8EUEzJihHEpoWyJ%2FDQBE0zDy3wLzypF12orT4qYtDclS5hFGEJElzAUQIAcqdljCcNULP3cD%2BRHRmNkvev3%2BvKYI7m2y7X8ZOpcjBiViAV4CU0k02COslQIIGfJjQQ%2BKo7dBPLVZPZM%2FG%2BuG9sTYFGHVlMIVvFew15joLkG0K4u%2Fhlgd9VK8wRedsLZCqheClPbYJOdcW3Z25ysq2Ev3t%2Fqy9RekTU4VOLv%2Bm8Zfs8I%2FxcQo9epMFZXYTa%2FVF%2BoqGg87q%2Bo9A1UdSWlWdC9NUFvQUnxjeB%2F5bDrnF%2B2c%2FasZ3bOtp5UG7bOfnt658HTAOwa58NtnP2d8X8wjfOwa5z%2F%2BS03WBP2Frzljo3wv%2FaA3rXPz2qfG5eWvbfPerMKts4u7K62NIv78Zq4H35t8U36%2Bb3smLqq0uaqohnr%2FVUVk1LuSkqjoPvrprQWlBST5jWCj2CIdOMu%2FRaxkFFATktpgCg8yX58lAqn14%2BIsxv2GdCFnsHLezGg8GteIjanm7SoJvfhyiQ%2FbDw8p2zGJ2hT0qozC10dtvWlmUc25nY1ma0nh20l5IgAgf%2BgGmg2TOBXGZTL6du169O3e2zVTeRnV085lZ87Vwx5K4acVUO5bwxDLzWb%2BybfPI5wEiO1XccPvRI%2FVCDojfghZ3C4BJFv0tMahB1HdLAckb%2BOEj9cjsg3qfCOI2radPXXhL0FTdcbE9HdKPecUa55Xdk%2FQWRyzx1B1LiwtJd89k3yuSOI2l5V9k4QDUzauSspzYLeXs55YHLORvD%2Ff4JIeXcrQaSz%2B2AIolVexx46dRO7EkSuu2JIY3oLQSRDCxYVNZVqjaZ3uSz%2FN5Crl%2F%2B%2BcE%2F%2FAg%3D%3D

