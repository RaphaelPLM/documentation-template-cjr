
## Descrição breve do método
> Insira abaixo qualquer explicação extra sobre o método. Caso o título resuma adequadamente a funcionalidade do método em questão, não é necessário fornecer qualquer descrição.

> Após a explicação acima, crie um subtítulo com a rota de acesso ao método, no formato abaixo:

>**[MÉTODO HTTP] URL_da_rota**
>Segue um exemplo abaixo: 
---
### [POST] /signin
> A seção "Requisição" deve ser obrigatoriamente preenchida quando for necessária para o processamento.
### Requisição:

> Forneça uma breve descrição de como deve ser feita a requisição para o método em questão. Explicite quais são os cabeçalhos necessários, bem como o corpo da requisição. Também indique qual o formato do objeto que deve ser enviado (JSON, Multipart Form, XML...).

> Modifique o campo abaixo com um exemplo do corpo ou cabeçalho da requisição. Se for necessário informar ambos, crie campos de código (como abaixo) separados, e identifique-os.
```
{
	"username": "raphael",
	"password": "123456"
}
```
> Feita a descrição da requisição, crie um subtítulo para cada possível resposta do método, no formato abaixo:

> **Resposta - CÓDIGO_STATUS_HTTP (Descrição do código)**
> Um exemplo é fornecido abaixo:
### Resposta - 200 (OK)

> Forneça uma breve descrição de quais campos serão retornados.

> Forneça também um objeto de exemplo. Deve-se atentar para não colocar informações sensíveis. Nesse caso forneça um valor fictício, que respeite o tipo do valor original (string, numérico, data, etc...). No exemplo abaixo, é um objeto JSON é ilustrado.

```
{
	"message": "Usuário autenticado com sucesso!",
	"token": "dishjsi92jh8h8f93hf8h389fh982"
}
```
---
